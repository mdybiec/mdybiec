/*
	Por�wnanie nazwisk

	Dlaczego nie u�ywa� algorytmu Levenshtein'a? 
	Nazwiska nie s� podobne do innych s��w, zazwyczaj s� dok�adnie pisane. W tym zastosowaniu nie wyst�puje problem odmiany przez przypadki, liczby itp.
	Podobne -- wg Levenshtein'a -- s�owa mog� by� r�nymi nazwiskami, np. Nowacki vs Nowicki; odleg�o�� Levenshtein'a wynosi jeden, a s� to r�ne nazwiska
	Niepodobne s�owa mog� by� podobnymi nazwiskami, np. G�rny vs Doli�czyk-G�rna; odleg�o�� Levenshtein'a wynosi 10 a mo�e by� i wi�ksza, zale�y od d�ugo�ci dodanego nazwiska.
	Podzia� nazwisk na cz�ci tak�e nie przybli�a do sukcesu, poniewa� r�nica mi�dzy G�rny a G�rna jet taka sama jak np. mi�dzy Bie�e� vs Gie�e�

	Poniewa� por�wnywane nazwiska s� w mianowniku, do rozwi�zania s� nast�puj�ce problemy:
	
	- Nazwiska dwucz�onowe; zazwyczaj s� kobiece, zatem praktycznie mo�na zaniedba� 
	  przypadek por�wnywania dw�ch nazwisk dwucz�onowych i je�eli nie s� identyczne, 
	  to traktowa� je jak zupe�nie r�ne.

	  Je�eli por�wnywane jest nazwisko dwucz�onowe z jednocz�onowym, wynik jest najmniejsz� 
	  odleg�o�ci� pomi�dzy nazwiskiem jednocz�onowym a dowolnym z cz�on�w dwucz�onowego.

	- Odmiana przez rodzaje. Po polsku jest to zamiana ostatniej litery m�skiego nazwiska 'i' lub 'y' na �e�skie 'a'.

	- Zamiana polskich liter na �aci�skie. W niekt�rych danych mo�liwe jest 
	  pomijanie polskich liter ze wzgl�du na ograniczenia wprowadzania danych. 
	  
	- Pomy�ki w pisowni mog� by� faktycznie pomy�kami lub rzeczywistymi r�nicami, jak w przyk�adach powy�ej, nie mo�na tego rozr�ni�.
	  
	Dzia�anie algorytmu jest nast�puj�ce:

	- Nazwiska dwucz�onowe s� dzielone na cz�ony, nast�pnie por�wnywane s� wszystkie mo�liwe pary cz�on�w nazwisk

~~~~
	Dla nazwisk:
	G�rny vs G�rna-Doli�czyk
	wykonywane s� nast�puj�ce por�wnania:

	G�rny vs G�rna
	G�rny vs Doli�czyk

	Dla nazwisk:
	Korwin-Mikke vs Korwin-Piotrowska
	wykonywane s� nast�puj�ce por�wnania

	Korwin vs Korwin
	Korwin vs Piotrowska
	Mikke vs Korwin
	Mikke vs Piotrowska

~~~~

	- Je�eli por�wnywane cz�ony nazwisk s� identyczne wynik wynosi 0

	- Je�eli jednen z cz�on�w z pierwszego nazwiska ko�czy si� na 'y' lub 'i' za� por�wnywany cz�on drugiego nazwiska na 'a' ale poza tym s� identyczne wynik wynosi 1

	- Je�eli nazwiska s� jednocz�onowe wyznaczanie indeksu jest zako�czone

	- Je�eli jedno z nazwisk jest dwucz�onowe, wynik jest powi�kszany o 10

	- Je�eli oba nazwiska s� dwucz�onowe wynik jest powi�kszany o 1000

	- Analogiczny algorytm jest wykonywany dla nazwisk, w kt�rych polskie litery zosta�y zamienione na ich �aci�skie odpowiedniki, przy czym otrzymany wynik jest powi�kszany o 50

	- Wybierany jest najmniejszy z otrzymanych indeks�w i jest on ko�cowym wynikiem por�wnania

~~~~
	Dzia�anie algorytmu dla przyk�adowych par nazwisk
	G�rny vs G�rna-Doli�czyk

	G�rny vs G�rna -- wynik wynosi 1
	G�rny vs Doli�czyk -- wynik wynosi 1000

	Jedno z nazwisk jest dwucz�onowe - wyniki s� powi�kszane o 10 i wynosz�: 11 oraz 1010.
	Por�wnanie nazwisk bez uwzgl�dniania polskich liter daje takie same rezultaty -- 1 i 1000;
	s� one powiekszane o 10 poniewa� jedno z nazwisk jest ducz�onowe -- 11 i 1010 
	oraz o 50 poniewa� por�wnanie bez polskich liter -- 61 i 1011
	Najmniejsza warto�� to 11 i jest to ko�cowy indeks podobie�stwa nazwisk.

	Korwin-Mikke vs Korwin-Piotrowska

	Korwin vs Korwin -- wynik wynosi 0
	Korwin vs Piotrowska -- wynik wynosi 1000
	Mikke vs Korwin -- wynik wynosi 1000
	Mikke vs Piotrowska -- wynik wynosi 1000

	Oba nazwiska s� dwucz�onowe -- wynik wynosi 1000, 2000, 2000, 2000
	Por�wnanie nazwisk bez uwzgl�dniania polskich liter daje takie same rezultaty;
	s� one powi�kszane o 50 poniewa� por�wnanie bez polskich liter -- 1050, 2050, 2050, 2050
	Najmniejsza warto�� to 1000 i jest to ko�cowy indeks podobie�stwa nazwisk.

	M�tewka vs Matewka -- wynik wynosi 1000 (nazwiska s� r�ne)

	Oba nazwiska s� jednocz�onowe - wynik nie jest powi�kszany i wynosi 1000
	Por�wnanie nazwisk bez uwzgl�dniania polskich liter:

	MATEWKA vs MATEWKA -- wynik wynosi 0 (nazwiska s� identyczne)
	jest on powi�kszany o 50 poniewa� por�wnanie bez polskich liter	-- 50
	Najmniejsza warto�� to 50 i jest to ko�cowy indeks podobie�stwa nazwisk.

~~~~


*/

-- Data wp�aty: 2017.12.24  Konto wp�aty: 26154012743019520450530001                OWCA-w�a�ciciel rach: 90508 ; Malgorzata Matewska  W�a�ciciel polisy: 74081401670 ; Sebastian M�tewski

declare @lnA varchar(50) =  
	'NOTUS Finanse S.A.' 
	--'G�RNY'
	--'G�rny'
	--'Korwin-Mikke'
declare @lnB varchar(50) =  
	'Mrozicka-Wilk'
	--'G�rna'
	--'G�rna-Doli�czyk'
	--'Korwin-Piotrowska'

		--select @lnA = LASTNAME
		--from MIS.dwh.CLIENT_DETAILS
		--where SECURITY_NO = '82020911050' and DWH_IS_CURRENT = 1
		
		--select @lnB = LASTNAME
		--from MIS.dwh.AGENT_DETAILS
		--where SECURITY_NO like '84071011765' and DWH_IS_CURRENT = 1

select @lnA, @lnB

select *,
'
<root>
	<DescriptionElement>
		<TemplatePos>01</TemplatePos>
		<ClientNo>123123</ClientNo>
		<PolicyNo>20987897</PolicyNo>
		<DataType>Lastname</DataType>
		<DataValue>' + lnA + '</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>02</TemplatePos>
		<ClientNo>45634</ClientNo>
		<PolicyNo>20654646</PolicyNo>
		<DataType>Lastname</DataType>
		<DataValue>' + lnB  + '</DataValue>
	</DescriptionElement>
</root>'
as ResultDescription
--select top 100 *
from 
(
select @lnB lnA, @lnA lnB
,
		(  /* Pocz�tek kodu obliczaj�cego indeks podobie�stwa nazwisk */
		select CASE WHEN plResVal > unResVal THEN unResVal ELSE plResVal END ResultValue
		from	(
				select
				MIN
					(CASE
					WHEN  S_lnamePart = C_lnamePart
						THEN '0' /* nazwiska s� identyczne wynik wynosi 0 */
					WHEN 
						CASE WHEN RIGHT(S_lnamePart,1) in ('i','y') THEN LEFT(S_lnamePart, LEN(S_lnamePart)-1) ELSE '#' END = 
						CASE WHEN RIGHT(C_lnamePart,1) in ('a') THEN LEFT(C_lnamePart, LEN(C_lnamePart)-1) ELSE '###' END
						THEN '1' /* jedno z nazwisk ko�czy si� na 'y' lub 'i' za� drugie na 'a' a poza tym s� identyczne wynik wynosi 1 */
					WHEN 
						CASE WHEN RIGHT(C_lnamePart,1) in ('i','y') THEN LEFT(C_lnamePart, LEN(C_lnamePart)-1) ELSE '###' END =
						CASE WHEN RIGHT(S_lnamePart,1) in ('a') THEN LEFT(S_lnamePart, LEN(S_lnamePart)-1) ELSE '#' END 
						THEN '1' /* jedno z nazwisk ko�czy si� na 'y' lub 'i' za� drugie na 'a' a poza tym s� identyczne wynik wynosi 1 */
					ELSE '1000' /* �aden z warunk�w dopasowuj�cych */
					END
					) +
				MIN
					(CASE
						WHEN S_P + C_P = 0 THEN 0
						WHEN S_P + C_P = 1 THEN 10
						ELSE CASE WHEN S_lnamePart = C_lnamePart THEN 0 ELSE 1000 END
					END
					) as plResVal,
				MIN
					(CASE
					WHEN  S_unPart = C_unPart
						THEN '0' /* nazwiska s� identyczne wynik wynosi 0 */
					WHEN 
						CASE WHEN RIGHT(S_unPart,1) in ('i','y') THEN LEFT(S_unPart, LEN(S_unPart)-1) ELSE '#' END = 
						CASE WHEN RIGHT(C_unPart,1) in ('a') THEN LEFT(C_unPart, LEN(C_unPart)-1) ELSE '###' END
						THEN '1' /* jedno z nazwisk ko�czy si� na 'y' lub 'i' za� drugie na 'a' a poza tym s� identyczne wynik wynosi 1 */
					WHEN 
						CASE WHEN RIGHT(C_unPart,1) in ('i','y') THEN LEFT(C_unPart, LEN(C_unPart)-1) ELSE '###' END =
						CASE WHEN RIGHT(S_unPart,1) in ('a') THEN LEFT(S_unPart, LEN(S_unPart)-1) ELSE '#' END 
						THEN '1' /* jedno z nazwisk ko�czy si� na 'y' lub 'i' za� drugie na 'a' a poza tym s� identyczne wynik wynosi 1 */
					ELSE '1000' /* �aden z warunk�w dopasowuj�cych */
					END
					) + 50 +
				MIN
					(
					CASE
						WHEN S_P + C_P = 0 THEN 0
						WHEN S_P + C_P = 1 THEN 10
						ELSE CASE WHEN S_unPart = C_unPart THEN 0 ELSE 1000 END
					END
					) as unResVal
				from
					(
					select 
					  LTRIM(RTRIM(lnamePart)) as S_lnamePart
					, LTRIM(RTRIM(lnamePart)) collate SQL_Latin1_General_CP1_CI_AI as S_unPart
					, LNP as S_P
					from	(
							select 
							  CONVERT(varchar(50), CASE WHEN CHARINDEX('-',@lnA) > 0  THEN LEFT(@lnA, CHARINDEX('-',@lnA)-1) ELSE @lnA END ) as LN1
							, CONVERT(varchar(50), CASE WHEN CHARINDEX('-',@lnA) > 0  THEN SUBSTRING(@lnA, CHARINDEX('-',@lnA)+1, 50) ELSE null END ) as LN2
							, SIGN(CHARINDEX('-',@lnA)) as LNP
							) as a
					unpivot (
							lnamePart FOR part IN (LN1, LN2)
							) as up
					) as a1
				cross join
					(
					select 
					  LTRIM(RTRIM(lnamePart)) as C_lnamePart
					, LTRIM(RTRIM(lnamePart)) collate SQL_Latin1_General_CP1_CI_AI as C_unPart
					, LNP as C_P
					from	(
							select 
							  CONVERT(varchar(50), CASE WHEN CHARINDEX('-',@lnB) > 0  THEN LEFT(@lnB, CHARINDEX('-',@lnB)-1) ELSE @lnB END ) as LN1
							, CONVERT(varchar(50), CASE WHEN CHARINDEX('-',@lnB) > 0  THEN SUBSTRING(@lnB, CHARINDEX('-',@lnB)+1, 50) ELSE null END ) as LN2
							, SIGN(CHARINDEX('-',@lnB)) as LNP
							) as b
					unpivot (
							lnamePart FOR part IN (LN1, LN2)
							) as up
					) as b1
				) as ab
		) as ResultValue /* Koniec kodu obliczaj�cego podobie�stwo nazwisk */
) a