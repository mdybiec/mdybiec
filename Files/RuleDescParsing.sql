delete from dbo.RuleResult

insert into dbo.RuleResult  (RuleExecID, ObjectType, ObjectID, ResultDescription, CreatedDate, CreatedBy) values
(10728, 'Policy', 'T1','
<root>
	<DescriptionElement>
		<TemplatePos>01</TemplatePos>
		<ClientNo>123123</ClientNo>
		<PolicyNo>20987897</PolicyNo>
		<DataType>PESEL</DataType>
		<DataValue>63031412345</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>02</TemplatePos>
		<ClientNo>123123</ClientNo>
		<PolicyNo>20987897</PolicyNo>
		<DataType>Firstname</DataType>
		<DataValue>Marcin</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>03</TemplatePos>
		<ClientNo>123123</ClientNo>
		<PolicyNo>20987897</PolicyNo>
		<DataType>Lastname</DataType>
		<DataValue>H�adki</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>04</TemplatePos>
		<ClientNo>123123</ClientNo>
		<DataType>Sensitive</DataType>
		<DataValue>syn Tadeusza</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>05</TemplatePos>
		<ClientNo/>
		<PolicyNo>20987897</PolicyNo>
		<DataType>NotSensitive</DataType>
		<DataValue>konsultant aktywny</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>06</TemplatePos>
		<ClientNo>34567</ClientNo>
		<DataType>Firstname</DataType>
		<DataValue>Janusz</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>07</TemplatePos>
		<ClientNo>34567</ClientNo>
		<DataType>Lastname</DataType>
		<DataValue>Boo</DataValue>
	</DescriptionElement>
</root>
',
GETDATE(), 'Test'),
(10727, 'Policy', 'T1','
<root>
	<DescriptionElement>
		<TemplatePos>01</TemplatePos>
		<ClientNo>098765</ClientNo>
		<PolicyNo>20123123</PolicyNo>
		<DataType>PESEL</DataType>
		<DataValue>11121408654</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>02</TemplatePos>
		<ClientNo>098765</ClientNo>
		<PolicyNo>20123123</PolicyNo>
		<DataType>Firstname</DataType>
		<DataValue>Zenon</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>03</TemplatePos>
		<ClientNo>098765</ClientNo>
		<PolicyNo>20123123</PolicyNo>
		<DataType>Lastname</DataType>
		<DataValue>M�tewka</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>04</TemplatePos>
		<ClientNo>098765</ClientNo>
		<DataType>Sensitive</DataType>
		<DataValue>syn Mariusza Ja�nickiego</DataValue>
	</DescriptionElement>
	<DescriptionElement>
		<TemplatePos>05</TemplatePos>
		<ClientNo/>
		<DataType>NotSensitive</DataType>
		<DataValue>klient aktywny</DataValue>
	</DescriptionElement>
</root>
',
GETDATE(), 'Test'),
(10726,'Policy','T2','Opis nie b�d�cy XMLem',GETDATE(), 'Test'),
(10730,'Payment','T3',null,GETDATE(),'Test')

delete from dbo.RuleResultDescription

insert into dbo.RuleResultDescription (RuleResultID, RuleID, TemplatePos, ClientNo, PolicyNo, DataType, DataValue) 
select rrd.RuleResultID, rrd.RuleID
, rrd.descElem.value('(/DescriptionElement/TemplatePos)[1]','int') as TemplatePos
, rrd.descElem.value('(/DescriptionElement/ClientNo)[1]','varchar(50)') as ClientNo
, rrd.descElem.value('(/DescriptionElement/PolicyNo)[1]','varchar(50)') as PolicyNo
, rrd.descElem.value('(/DescriptionElement/DataType)[1]','varchar(50)') as DataType
, rrd.descElem.value('(/DescriptionElement/DataValue)[1]','varchar(50)') as DataValue
from (
	select rrd.RuleResultID, rrd.RuleID, rrd2.dsc.query('.') descElem
	from	(
			select rrd.RuleResuItID as RuleResultID, rl.RuleID 
			, CAST(
				CASE isnull(CAST(rrd.ResultDescription as XML).exist('/root/DescriptionElement'),0)
					WHEN 0 THEN '<root><DescriptionElement><DataType>NotSensitive</DataType><DataValue>'+isnull(rrd.ResultDescription,'NULL')+'</DataValue></DescriptionElement></root>'
					ELSE rrd.ResultDescription
				END 
			  as xml ) as RuleDescriptionXML
			from dbo.RuleResult rrd
			join dbo.RuleExec rx on rrd.RuleExecID = rx.RuleExecID
			join dbo.[Rule] rl on rx.RuleID = rl.RuleID
			) rrd
	CROSS APPLY RuleDescriptionXML.nodes('/root/DescriptionElement') as rrd2(dsc)
	) as rrd


	select * from dbo.RuleResultDescription