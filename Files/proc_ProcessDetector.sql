﻿--IF OBJECT_ID('RulesRepository.proc_ProcessDetector') IS NOT NULL DROP PROC RulesRepository.proc_ProcessDetector
--GO

CREATE PROCEDURE RulesRepository.proc_ProcessDetector
	@pDetectorID INT, @pRunBy NVARCHAR(50) = NULL, @pRunMode NVARCHAR(50) = 'AUTO'
AS
DECLARE 
	@RuleSQL NVARCHAR(max), 
	@RuleID INT, 
    @GroupID INT,
    @DeltaID INT,
	@DetectorID INT,
    @DetectorExecID INT,
    @RuleExecID INT,
	@SQL NVARCHAR(max), 
	@Msg NVARCHAR(max),
    @ObjectType NVARCHAR(50),
    @DeltaSQL NVARCHAR(max),
    @DeltaExecID INT,
    @ErrorBehavior NVARCHAR(50),
    @DetectorRunStatus NVARCHAR(50) = dbo.func_GetConst('RunStatus_OK'),
    @DeltaRunStatus NVARCHAR(50),
	@RowsProcessed INT, 
    @MaxDWHLoadDate DATETIME,
    @LastDeltaDWHDate DATETIME,
    @ErrorOccurred INT = 0

IF @pRunBy IS NULL SET @pRunBy = SUSER_NAME()

SET @DeltaSQL = 'SELECT ObjectID FROM tempdb.dbo.AF_ActiveDelta'

-- pobranie atrybutów detektora
SELECT @DeltaID = d.DeltaID, @ErrorBehavior = d.ErrorBehavior
FROM dbo.Detector d 
WHERE d.DetectorID = @pDetectorID

IF @DeltaID IS NULL
BEGIN
    RAISERROR('Brak delty lub detektora',15,1)
    RETURN
END

-- jesli tryb automatyczny, tzn. uruchomiony z joba
IF @pRunMode = 'AUTO' 
BEGIN
	INSERT INTO dbo.DetectorExec(DetectorID, DeltaExecID, CreatedDate, CreatedBy, RunStatus, RunMode)
	SELECT @pDetectorID, null, GETDATE(), SUSER_NAME(), dbo.func_GetConst('RunStatus_RUNNING'), @pRunMode

	SELECT @DetectorExecID = @@IDENTITY
END
ELSE -- jesli tryb ręczny - bierzemy z kolejki
BEGIN
	SELECT @DetectorExecID = DetectorExecID
	FROM dbo.DetectorExec d 
	WHERE RunStatus = dbo.func_GetConst('RunStatus_WAITING')
		AND DetectorID = @pDetectorID

    UPDATE dbo.DetectorExec 
    SET RunStatus = dbo.func_GetConst('RunStatus_RUNNING')
    WHERE DetectorExecID = @DetectorExecID
END

BEGIN TRY
    -- odświeżenie danych niesystemowych
    EXEC ReferenceData.proc_LoadDataToDWH
END TRY
BEGIN CATCH
    SET @ErrorOccurred = 1
END CATCH

-- przygotowanie delty
EXEC RulesRepository.proc_PrepareDelta @DeltaID, @DeltaExecID OUTPUT, @pRunBy, @DeltaRunStatus OUTPUT, @MaxDWHLoadDate OUTPUT, @LastDeltaDWHDate OUTPUT

UPDATE dbo.DetectorExec
SET DeltaExecID = @DeltaExecID
WHERE DetectorExecID = @DetectorExecID

-- jeśli wystąpił jakiś problem w odświeżaniu delty, ustawiamy error na detektorze
IF @DeltaExecID IS NULL OR @DeltaRunStatus = dbo.func_GetConst('RunStatus_ERROR') OR @ErrorOccurred = 1
BEGIN
    SET @DetectorRunStatus = dbo.func_GetConst('RunStatus_ERROR')
END
ELSE
BEGIN
    -- detector był już uruchamiany na konkretnej delcie, nie liczymy ponownie, reguły też się nie uruchamiają
    IF EXISTS(
        SELECT 1 
        FROM dbo.DetectorExec 
        WHERE DetectorID = @pDetectorID 
            AND DeltaExecID = @DeltaExecID 
            AND DetectorExecID <> @DetectorExecID
            AND RunStatus = dbo.func_GetConst('RunStatus_OK')
            ) 
	    AND @pRunMode = 'AUTO'
    BEGIN
        SET @DetectorRunStatus = dbo.func_GetConst('RunStatus_OK')
    END
    ELSE
    BEGIN
        -- pobranie reguł do przetworzenia
        DECLARE RuleCrs CURSOR FOR 
	    SELECT d.DetectorID, r.RuleID, rg.GroupID, r.RuleCodeSQL, r.ObjectType
	    FROM Detector d
        JOIN RuleGroup rg ON rg.GroupID = d.GroupID
	    JOIN [Rule] r ON r.RuleID = rg.RuleId AND r.RuleType = 'SQL'
	    WHERE d.DetectorIsActive = 'Y' 
		    AND r.RuleIsActive = 'Y'
		    AND d.DetectorID = @pDetectorID
	    ORDER BY rg.RuleGroupSeq

	    OPEN RuleCrs

	    FETCH NEXT FROM RuleCrs INTO @DetectorID, @RuleID, @GroupID, @RuleSQL, @ObjectType

        -- przetwarzamy reguły po kolei
	    WHILE @@FETCH_STATUS = 0 
	    BEGIN

            INSERT INTO dbo.RuleExec(DetectorExecID,RuleID,CodeSQLExecuted,RunStatus,ErrorMsg,CreatedDate,CreatedBy)
            VALUES (@DetectorExecID,@RuleID,null,dbo.func_GetConst('RunStatus_RUNNING'),null,GETDATE(),@pRunBy)
            SELECT @RuleExecID = @@IDENTITY

            -- sprawdzenie czy kod reguły jest podany poprawnie czy nie
            IF @RuleSQL IS NULL 
            BEGIN
                SET @Msg = 'Kod reguły jest pusty'

                UPDATE dbo.RuleExec
			    SET RunStatus = dbo.func_GetConst('RunStatus_ERROR'), ErrorMsg = @Msg
			    WHERE RuleExecID = @RuleExecID
            END
            ELSE
            BEGIN           
                -- preprocessing, usuwamy średniki z końca SQLa (DEV ONLY)
                SET @RuleSQL = REPLACE(@RuleSQL,';','')

                DECLARE @RuleResultColumns NVARCHAR(max)
                SET @RuleResultColumns = 'ObjectID'
                IF(@RuleSQL LIKE '%AssocObjectID1%')    SET @RuleResultColumns += ', AssocObjectID1'    ELSE SET @RuleResultColumns += ', null AssocObjectID1' 
                IF(@RuleSQL LIKE '%AssocObjectID2%')    SET @RuleResultColumns += ', AssocObjectID2'    ELSE SET @RuleResultColumns += ', null AssocObjectID2' 
                IF(@RuleSQL LIKE '%AssocObjectID3%')    SET @RuleResultColumns += ', AssocObjectID3'    ELSE SET @RuleResultColumns += ', null AssocObjectID3' 
                IF(@RuleSQL LIKE '%ResultValue%')       SET @RuleResultColumns += ', ResultValue'       ELSE SET @RuleResultColumns += ', null ResultValue' 
                IF(@RuleSQL LIKE '%ResultDescription%') SET @RuleResultColumns += ', ResultDescription' ELSE SET @RuleResultColumns += ', null ResultDescription'             

			    -- wstaw parametry dat w kod reguły
			    SET @RuleSQL = REPLACE(@RuleSQL,'@AFLastProcessingDate','CONVERT(DATETIME, ''' + CONVERT(NVARCHAR(max),COALESCE(@LastDeltaDWHDate,DATEADD(day,-7,GETDATE())),120) + ''', 120)')
			    SET @RuleSQL = REPLACE(@RuleSQL,'@MaxDWHLoadDate','CONVERT(DATETIME, ''' + COALESCE(CONVERT(NVARCHAR(max),@MaxDWHLoadDate,120),CONVERT(NVARCHAR(max),'1900-01-01')) + ''', 120)')

                -- przygotujmy SQL do wykonania reguły. reguła jest wykonywana na bazie DWH.
		        SET @SQL = 'SELECT ' + @RuleResultColumns +' FROM(' + @RuleSQL + ') tmp WHERE ObjectID IN(' + @DeltaSQL +')'
                --PRINT @SQL

			    UPDATE dbo.RuleExec
			    SET CodeSQLExecuted = @SQL
			    WHERE RuleExecID = @RuleExecID

		        BEGIN TRY
                    -- tabela tymczasowa na wyniki reguły
				    IF (OBJECT_ID('tempdb..#Rule_Output') IS NOT NULL) DROP TABLE #Rule_Output
                    SELECT TOP 0 ObjectID, AssocObjectID1, AssocObjectID2, AssocObjectID3, ResultValue, ResultDescription 
                    INTO #Rule_Output 
                    FROM dbo.RuleResult                

                    -- wykonanie reguły na zdalnym serwerze i wstawienie wyników lokalnie
                    INSERT INTO #Rule_Output(ObjectID, AssocObjectID1, AssocObjectID2, AssocObjectID3, ResultValue, ResultDescription)			    
                    EXEC(@SQL) AT [AF_DWH] 

                    INSERT INTO dbo.RuleResult(ObjectID, AssocObjectID1, AssocObjectID2, AssocObjectID3, ResultValue, ResultDescription, RuleExecID, ObjectType, CreatedDate, CreatedBy)			    
                    SELECT ObjectID, AssocObjectID1, AssocObjectID2, AssocObjectID3, ResultValue, ResultDescription, @RuleExecID DeltaRuleDetectorExecID, @ObjectType ObjectType, GETDATE() CreatedDate, SUSER_NAME() CreatedBy
                    FROM #Rule_Output
				    SET @RowsProcessed = @@ROWCOUNT

                    -- jeśli wszystko zostało wykonane poprawnie logujemy sukces oraz przenosimy wyniki tymczasowe do wyników reguł
				    UPDATE dbo.RuleExec
				    SET RunStatus = dbo.func_GetConst('RunStatus_OK'), RowsProcessed = @RowsProcessed
				    WHERE RuleExecID = @RuleExecID
		        END TRY
		        BEGIN CATCH
			        SET @Msg = ERROR_MESSAGE()

				    UPDATE dbo.RuleExec
				    SET RunStatus = dbo.func_GetConst('RunStatus_ERROR'), ErrorMsg = @Msg
				    WHERE RuleExecID = @RuleExecID

				    -- jesli detektor ma ustawione, żeby przerywać przetwarzanie przy błędzie reguły - przerywamy
                    IF @ErrorBehavior = dbo.func_GetConst('ErrorBehavior_FAIL')
                    BEGIN
                        SET @DetectorRunStatus = dbo.func_GetConst('RunStatus_ERROR')
                        BREAK
                    END
		        END CATCH
            END

    	    FETCH NEXT FROM RuleCrs INTO @DetectorID, @RuleID, @GroupID, @RuleSQL, @ObjectType

	    END

	    CLOSE RuleCrs
	    DEALLOCATE RuleCrs
    END
END

UPDATE dbo.DetectorExec 
SET RunStatus = @DetectorRunStatus
WHERE DetectorExecID = @DetectorExecID
    
PRINT 'Detector ' + CONVERT(NVARCHAR, @pDetectorID) + ' status: ' + @DetectorRunStatus
GO


