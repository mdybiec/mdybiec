
;with pdd
as	(
	select	CASE
				WHEN CHARINDEX('%s', rl.RuleDescTemplate) = 0
					THEN rl.RuleDescTemplate + CHAR(13)+CHAR(10)+isnull(DataValue,'NULL')
				ELSE STUFF(rl.RuleDescTemplate, CHARINDEX('%s',rl.RuleDescTemplate), 2, isnull(DataValue,'')) 
			END as dsc
			, isnull(TemplatePos,1) pos , DataValue value, RuleResultID rrid, rrd.RuleID
			, CASE WHEN TemplatePos is null THEN 0 ELSE 1 END as isXMLelement
		from dbo.RuleResultDescription rrd
		join	(
				select RuleID
				, coalesce(ResultDescriptionTemplate,
				  CHAR(13)+CHAR(10)+'Generowany opis:'+REPLICATE(CHAR(13)+CHAR(10)+' - %s',100)
				  ) as RuleDescTemplate
				from dbo.[Rule] 
				) as rl 
			on rl.RuleID = rrd.RuleID
		where isnull(TemplatePos,1) = 1

	union all

	select	CASE
				WHEN CHARINDEX('%s', pdd.dsc) = 0 
					THEN pdd.dsc + CHAR(13)+CHAR(10)+' - '+isnull(pd.DataValue,'NULL')
				ELSE STUFF(pdd.dsc, CHARINDEX('%s', pdd.dsc), 2, isnull(pd.DataValue,'NULL')) 
			END as dsc
			, pd.TemplatePos pos, pd.DataValue value, pd.RuleResultID rrid, pdd.RuleID
			, pdd.isXMLelement
		from pdd
		join dbo.RuleResultDescription pd on pdd.pos + 1 = isnull(pd.TemplatePos,1) and pdd.rrid = pd.RuleResultID
	)

select rr.RuleResuItID, pdd.RuleID, pdd.pos, pdd.isXMLelement
, coalesce(LTRIM(REPLACE(pdd.dsc,CHAR(13)+CHAR(10)+' - %s','')), rr.ResultDescription, 'NULL') as ResultDescription
, rr.ObjectID, rr.ObjectType, rr.CreatedDate, rr.CreatedBy -- dowolne obiekty i joiny
from	dbo.RuleResult rr
join	(
		select rrid, dsc, pos, RuleID, isXMLelement
		, MAX(pos) over (partition by rrid) rn
		from pdd
		) pdd
on rr.RuleResuItID = pdd.rrid and rn = pos
